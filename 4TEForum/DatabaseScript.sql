
DROP DATABASE IF EXISTS [Forum4TE-Decker]
GO
CREATE DATABASE [Forum4TE-Decker]
GO

USE [Forum4TE-Decker]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tb_Usuarios] (
    [Id]    INT           IDENTITY (1, 1) NOT NULL,
    [Nome]  NVARCHAR (50) NOT NULL,
    [Email] NVARCHAR (50) NOT NULL,
    [Senha] NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE TABLE [dbo].[tb_Foruns] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [IncluidoPor]   INT            NOT NULL,
    [IncluidoEm]    DATETIME       NOT NULL,
    [Titulo]        NVARCHAR (50)  NOT NULL,
    [Conteudo]      NVARCHAR (MAX) NOT NULL,
    [RespostaForum] INT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Foruns_ToUsuarios] FOREIGN KEY ([IncluidoPor]) REFERENCES [dbo].[tb_Usuarios] ([Id]),
    CONSTRAINT [FK_tb_Foruns_ToForuns] FOREIGN KEY ([RespostaForum]) REFERENCES [dbo].[tb_Foruns] ([Id])
);

GO
CREATE NONCLUSTERED INDEX [IX_tb_Foruns_Id]
    ON [dbo].[tb_Foruns]([Id] ASC);

SET IDENTITY_INSERT [dbo].[tb_Usuarios] ON 
INSERT [dbo].[tb_Usuarios] ([Id], [Nome], [Email], [Senha]) VALUES (1, 	'Andr�', 'andre@ficticio123.com', 'andre123')
INSERT [dbo].[tb_Usuarios] ([Id], [Nome], [Email], [Senha]) VALUES (2, 	'Rita', 'rita@ficticio123.com', 'rita123')
INSERT [dbo].[tb_Usuarios] ([Id], [Nome], [Email], [Senha]) VALUES (3, 	'Vanessa', 'vanessa@ficticio123.com', 'vanessa123')
INSERT [dbo].[tb_Usuarios] ([Id], [Nome], [Email], [Senha]) VALUES (4, 	'Daniel', 'daniel@ficticio123.com', 'daniel123')
INSERT [dbo].[tb_Usuarios] ([Id], [Nome], [Email], [Senha]) VALUES (5, 	'Ricardo', 'ricardo@ficticio123.com', 'ricardo123')
SET IDENTITY_INSERT [dbo].[tb_Usuarios] OFF
GO

SET IDENTITY_INSERT [dbo].[tb_Foruns] ON 
INSERT [dbo].[tb_Foruns] ([Id], [IncluidoPor], [IncluidoEm], [Titulo], [Conteudo]) VALUES (1, 5, '13/06/2022 12:00:01', 'Profundidade t�cnica', 'Tamb�m � uma �tima op��o para uma diversidade de temas que s�o muito abrangentes ou com muitos desdobramentos, como por exemplo, tecnologia, tanto que um dos f�runs brasileiros mais antigos e conhecidos, � justamente sobre o tema � Clube do Hardware. F�runs como esse, conseguem tanto abra�ar aqueles totalmente leigos, que carecem de apenas um dado simples contido no manual impresso que j� n�o se tem, ou mesmo um amplo conjunto de informa��es que as vezes n�o � poss�vel encontrar em nenhum outro site, para um profissional da �rea. Lembre-se que quanto maior o f�rum em termos de participantes, s�o mais pessoas com diferentes n�veis de conhecimento e que muitas vezes est�o dispostos compartilh�-las, afinal essa � outra caracter�stica dos f�runs � seus participantes voluntariamente compartilham o que sabem. Trocam conhecimento e experi�ncias.')
INSERT [dbo].[tb_Foruns] ([Id], [IncluidoPor], [IncluidoEm], [Titulo], [Conteudo]) VALUES (2, 3, '13/06/2022 12:01:02', 'Maior relacionamento', 'Nos casos de f�runs ligados a empresas, dependendo de como � conduzido, ainda h� o Marketing de Relacionamento que pode ser praticado. � comum nesse tipo, alguns representantes da organiza��o que t�m o papel de ser a voz da marca, dando respostas sobre d�vidas que s� a empresa pode esclarecer, pol�ticas, planos futuros, quest�es t�cnicas, etc. Nesse momento, � a empresa falando diretamente aos seus clientes. Mas h� tamb�m muitos exemplos de mudan�as em produtos e at� mesmo a cria��o de novos, como resultado da intera��o dos usu�rios em seus f�runs, seja por meio de pedidos, sugest�es e propostas que visam dar solu��es para dores dos clientes. O melhor disso, � que as novidades que surgem como consequ�ncia dessa intera��o empresa / cliente, nasce de quem consome o produto ou recebe o servi�o.')
INSERT [dbo].[tb_Foruns] ([Id], [IncluidoPor], [IncluidoEm], [Titulo], [Conteudo]) VALUES (3, 4, '13/06/2022 12:01:12', 'Conhecimento do p�blico', 'Entre todas as caracter�sticas de um f�rum, aquela que talvez seja a mais importante para uma empresa que decida por criar um, seja o conhecimento que ele proporciona dos seus clientes e que muitos n�o d�o o devido valor. Um f�rum bem administrado e moderado � entenda-se aqui, n�o censurado � prov� informa��es valiosas do dia a dia dos usu�rios e como eles se relacionam com a informa��o que fornecem e consomem no f�rum, o que muitas vezes nem as pesquisas mais extensivas s�o capazes de fornecer. Em muitos � poss�vel ver depoimentos, relatos dos clientes, em situa��es quotidianas, algumas delas com riqueza de detalhes, as quais permitem um conhecimento profundo do p�blico. Quando � o caso de um f�rum mantido por uma empresa, ela fica sabendo por exemplo, o uso que se faz do produto, em que condi��es os problemas acontecem, quais as dificuldades de manipula��o, entre muitos dados importantes e que s�o fornecidos de modo espont�neo.')
INSERT [dbo].[tb_Foruns] ([Id], [IncluidoPor], [IncluidoEm], [Titulo], [Conteudo]) VALUES (4, 1, '13/06/2022 12:02:55', 'Pluralidade de conte�dos', 'Outro ponto importante que faz uso das caracter�sticas dos f�runs, � a pluralidade e o volume de conte�dos e o seu car�ter colaborativo. F�runs antigos e com bom n�mero de usu�rios, s�o uma fonte inesgot�vel de informa��o, produzida em sua maioria por quem consome tal conte�do e, portanto, para quem e por quem mais importa � o visitante e que � na maioria das vezes, tamb�m o cliente. Os conte�dos nascem e s�o desenvolvidos com base na experi�ncia pr�tica que o cliente tem em rela��o aos produtos, servi�os e temas sobre os quais o respectivo f�rum � dedicado. Contrariamente a sites que t�m um � ou at� alguns � administrador respons�vel por decidir sobre seu conte�do, por mais criativo e conhecedor de um assunto que ele seja, v�rias mentes conseguem vislumbrar e propor assuntos mais variados relacionados ao tema central.')
INSERT [dbo].[tb_Foruns] ([Id], [IncluidoPor], [IncluidoEm], [Titulo], [Conteudo]) VALUES (5, 2, '13/06/2022 12:05:12', 'O visitante tem voz', 'O primeiro ponto a observarmos, � que existe resist�ncia das empresas em adot�-lo porque exige que a empresa tenha uma boa imagem ou reputa��o digital, qualidade no atendimento e qualidade associada aos seus produtos / servi�os. Dada a liberdade que normalmente h� quanto a participa��o de qualquer pessoa, se a empresa n�o tem seguran�a quanto �s opini�es dos seus clientes, o f�rum pode tornar-se um canal de reclama��es e consequentemente comprometer ainda mais sua imagem. Sim, a participa��o externa e indiscriminada � a principal caracter�stica de um f�rum, onde a voz dos visitantes � �ouvida� por sua participa��o. Mas se por um lado os clientes t�m liberdade para falar o que quiserem � bem ou mal � da sua empresa, hoje h� uma variedade de canais na Internet nos quais eles podem faz�-lo, independente do controle e da vontade da empresa. Se o cliente tem motivos para falar mal, n�o � esse o caminho para mudar a situa��o. Logo, esse n�o deve ser um fator restritor. Ao contr�rio, em um f�rum da pr�pria empresa, o acesso �s informa��es dos seus clientes � maior, bem como sua intera��o, j� que administradores, moderadores e mesmo clientes satisfeitos, podem contribuir para reverter posi��es, resolver problemas e sanar d�vidas. Visto de outra forma, � mais um meio de resolver poss�veis dores dos clientes.')
SET IDENTITY_INSERT [dbo].[tb_Foruns] OFF
GO
USE [master]
GO
ALTER DATABASE [Forum4TE-Decker] SET  READ_WRITE 
GO