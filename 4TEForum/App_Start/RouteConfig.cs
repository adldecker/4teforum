﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace _4TEForum
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Forum", "Forum/Adiciona", new { controller = "Forum", action = "Adiciona" });
            routes.MapRoute("VisualizaForum", "Forum/Visualiza/:id", new { controller = "Forum", action = "Visualiza", id = "" });
            routes.MapRoute("SalvarForum", "Forum/SalvarForum", new { controller = "Forum", action = "SalvarForum" });
            routes.MapRoute("EdicaoForum", "Forum/EditarForum/:id", new { controller = "Forum", action = "EditarForum", id = "" });
            routes.MapRoute("ExclusaoForum", "Forum/Exclui/:id", new { controller = "Forum", action = "ExcluirForum", id = "" });

            routes.MapRoute("UsuariosRegistrar", "RegistraUsuario", new { controller = "Usuarios", action = "RegistraUsuario" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
