﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace _4TEForum.Models
{
    public class Usuarios
    {
        private readonly static string _conn = WebConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        public int Id { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }

        public bool Login()
        {
            var result = false;
            var sql = "SELECT ID, NOME FROM TB_USUARIOS WHERE EMAIL = @EMAIL AND SENHA = @SENHA";

            try
            {
                using (var cn = new SqlConnection(_conn))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand(sql, cn))
                    {
                        cmd.Parameters.AddWithValue("@EMAIL", Email);
                        cmd.Parameters.AddWithValue("@SENHA", Senha);
                        using (var dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                Id = Convert.ToInt32(dr["ID"]);
                                Nome = dr["NOME"].ToString();
                                result = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha no login: {ex.Message}");
            }

            return result;
        }

        internal void CadastraUsuario()
        {
            var sql = "INSERT INTO TB_USUARIOS (NOME, EMAIL, SENHA) VALUES (@NOME, @EMAIL, @SENHA)";

            try
            {
                using (var cn = new SqlConnection(_conn))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand(sql, cn))
                    {
                        cmd.Parameters.AddWithValue("@NOME", Nome);
                        cmd.Parameters.AddWithValue("@EMAIL", Email);
                        cmd.Parameters.AddWithValue("@SENHA", Senha);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha no login: {ex.Message}");
            }
        }

        internal bool Cadastrado()
        {
            var result = false;
            var sql = "SELECT NOME FROM TB_USUARIOS WHERE EMAIL = @EMAIL";

            try
            {
                using (var cn = new SqlConnection(_conn))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand(sql, cn))
                    {
                        cmd.Parameters.AddWithValue("@EMAIL", Email);
                        using (var dr = cmd.ExecuteReader())
                            result = dr.HasRows;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha no login: {ex.Message}");
            }

            return result;
        }
    }
}