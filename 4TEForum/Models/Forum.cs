﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace _4TEForum.Models
{
    public class Forum
    {
        private readonly static string _conn = WebConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        #region Propriedades
        public int Id { get; set; }
        public string IncluidoPorNome { get; set; }
        public int IncluidoPorId { get; set; }
        public DateTime IncluidoEm { get; set; }
        public string Titulo { get; set; }
        public string Conteudo { get; set; }
        public long ForumResposta { get; set; }
        public bool Proprietario { get; set; }
        #endregion

        #region Construtores
        public Forum() { }

        public Forum(int id, string incluidoPor, DateTime incluidoEm, string titulo, string conteudo, long forumResposta, bool proprietario)
        {
            Id = id;
            IncluidoPorNome = incluidoPor;
            IncluidoEm = incluidoEm;
            Titulo = titulo;
            Conteudo = conteudo;
            ForumResposta = forumResposta;
            Proprietario = proprietario;
        }
        #endregion

        #region Métodos
        public static List<Forum> RecuperaForuns(int usuarioLogado)
        {
            var foruns = new List<Forum>();
            var sql = @"SELECT A.ID, B.NOME, A.INCLUIDOEM, A.TITULO, A.CONTEUDO, A.INCLUIDOPOR
                          FROM TB_FORUNS A INNER JOIN 
                               TB_USUARIOS B ON A.INCLUIDOPOR = B.ID
                         WHERE A.RESPOSTAFORUM IS NULL";

            try
            {
                using (var conn = new SqlConnection(_conn))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(sql, conn))
                    {
                        using (var dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                foruns.Add(new Forum(
                                    Convert.ToInt32(dr["ID"]),
                                    dr["NOME"].ToString(),
                                    Convert.ToDateTime(dr["INCLUIDOEM"]),
                                    dr["TITULO"].ToString(),
                                    dr["CONTEUDO"].ToString(),
                                    0,
                                    Convert.ToInt32(dr["INCLUIDOPOR"]) == usuarioLogado));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha: {ex.Message}");
            }

            return foruns;
        }

        internal void ExcluiForum()
        {
            ExcluiDados($"DELETE TB_FORUNS WHERE RESPOSTAFORUM = {Id}");
            ExcluiDados($"DELETE TB_FORUNS WHERE ID = {Id}");
        }

        private void ExcluiDados(string sql)
        {
            try
            {
                using (var conn = new SqlConnection(_conn))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(sql, conn))
                        cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha: {ex.Message}");
            }
        }

        public void RecuperaForum(int id, int usuarioLogado)
        {
            var sql = $@"SELECT A.ID, B.NOME, A.INCLUIDOEM, A.TITULO, A.CONTEUDO, A.INCLUIDOPOR
                        FROM TB_FORUNS A INNER JOIN 
                             TB_USUARIOS B ON A.INCLUIDOPOR = B.ID 
                        WHERE A.ID = {id}";
            try
            {
                using (var conn = new SqlConnection(_conn))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(sql, conn))
                    using (var dr = cmd.ExecuteReader())
                        if (dr.Read())
                        {
                            Id = Convert.ToInt32(dr["ID"]);
                            IncluidoPorNome = dr["NOME"].ToString();
                            IncluidoEm = Convert.ToDateTime(dr["INCLUIDOEM"].ToString());
                            Titulo = dr["TITULO"].ToString();
                            Conteudo = dr["CONTEUDO"].ToString();
                            Proprietario = Convert.ToInt32(dr["INCLUIDOPOR"]) == usuarioLogado;
                        }
                }
            }
            catch (Exception ex)
            {
                Titulo = $"Falha: {ex.Message}";
                Console.WriteLine(Titulo);
            }
        }

        public void EditarForum()
        {
            var sql = $"UPDATE TB_FORUNS SET TITULO = @Titulo, CONTEUDO = @Conteudo WHERE ID = {Id}";

            try
            {
                using (var cn = new SqlConnection(_conn))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand(sql, cn))
                    {
                        cmd.Parameters.AddWithValue("@Titulo", Titulo);
                        cmd.Parameters.AddWithValue("@Conteudo", Conteudo);
                        cmd.Parameters.AddWithValue("@Id", Id);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha: {ex.Message}");
            }
        }

        public void SalvarForum()
        {
            var sql = "INSERT INTO TB_FORUNS (TITULO, CONTEUDO, INCLUIDOPOR, INCLUIDOEM) " +
                      " VALUES (@Titulo, @Conteudo, @IncluidoPor, @IncluidoEm)";

            try
            {
                using (var cn = new SqlConnection(_conn))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand(sql, cn))
                    {
                        cmd.Parameters.AddWithValue("@Titulo", Titulo);
                        cmd.Parameters.AddWithValue("@Conteudo", Conteudo);
                        cmd.Parameters.AddWithValue("@IncluidoPor", IncluidoPorId);
                        cmd.Parameters.AddWithValue("@IncluidoEm", IncluidoEm);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha: {ex.Message}");
            }
        }
        #endregion
    }
}