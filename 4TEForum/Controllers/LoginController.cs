﻿using System.Web.Mvc;

namespace _4TEForum.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            if (Session["ErroLogin"] != null)
                ViewBag.ErroLogin = Session["ErroLogin"].ToString();

            ViewBag.Title = "Login";

            return View();
        }

        [HttpPost]
        public void ChecarLogin()
        {
            Models.Usuarios usuario = new Models.Usuarios();
            usuario.Email = Request["Email"];
            usuario.Senha = Request["Senha"];

            if (usuario.Login())
            {
                Session["UsuarioLogado"] = usuario;
                Session["NomeUsuario"] = usuario.Nome;
                Session.Remove("ErroLogin");
                Response.Redirect("/Inicio/Forum");
            }
            else
            {
                Session["ErroLogin"] = "Usuário ou senha incorretos";
                Response.Redirect("/Login/Index");
            }
        }
    }
}