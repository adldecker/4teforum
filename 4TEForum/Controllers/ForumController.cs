﻿using System;
using System.Web.Mvc;

namespace _4TEForum.Controllers
{
    public class ForumController : Controller
    {
        public ActionResult Visualiza(string id)
        {
            if (Session["UsuarioLogado"] != null)
            {
                ViewBag.Forum = RecuperaForum(id);
                ViewBag.Title = ViewBag.Forum.Titulo;
                return View();
            }
            else
                return RedirectToAction("Index", "Login");
        }

        public ActionResult Adiciona()
        {
            if (Session["UsuarioLogado"] != null)
            {
                ViewBag.Title = "Criação de forum";
                ViewBag.Message = ViewBag.Title;
                return View();
            }
            else
                return RedirectToAction("Index", "Login");
        }

        public ActionResult Editar(string id)
        {
            if (Session["UsuarioLogado"] != null)
            {
                ViewBag.Forum = RecuperaForum(id);
                ViewBag.Title = $"Editar \"{ViewBag.Forum.Titulo}\"";
                ViewBag.Message = ViewBag.Title;

                return View();
            }
            else
                return RedirectToAction("Index", "Login");
        }

        public ActionResult Exclui(string id)
        {
            if (Session["UsuarioLogado"] != null)
            {
                ViewBag.Forum = RecuperaForum(id);
                ViewBag.Title = "Confirmação";
                ViewBag.Message = $"Excluir o forum \"{ViewBag.Forum.Titulo}\"?";

                return View();
            }
            return RedirectToAction("Index", "Login");
        }

        [HttpPost]
        public void SalvarForum()
        {
            if (Session["UsuarioLogado"] != null)
            {
                Models.Forum forum = new Models.Forum
                {
                    Titulo = Request["titulo"],
                    Conteudo = Request["conteudo"],
                    IncluidoEm = DateTime.Now,
                    IncluidoPorId = (Session["UsuarioLogado"] as Models.Usuarios).Id
                };

                forum.SalvarForum();
                Response.Redirect("/Inicio/Forum");
            }
            else
                Response.Redirect("/Login/Index");
        }

        [HttpPost]
        public void EditarForum()
        {
            if (Session["UsuarioLogado"] != null)
            {
                Models.Forum forum = new Models.Forum
                {
                    Id = RecuperaForumId(Request["id"]),
                    Titulo = Request["titulo"],
                    Conteudo = Request["conteudo"],
                    IncluidoPorId = (Session["UsuarioLogado"] as Models.Usuarios).Id,
                    IncluidoEm = DateTime.Now.ToUniversalTime(),
                    ForumResposta = 0
                };

                forum.EditarForum();
                Response.Redirect("/Inicio/Forum");
            }
            else
                Response.Redirect("/Login/Index");
        }

        [HttpPost]
        public void ExcluirForum()
        {
            if (Session["UsuarioLogado"] != null)
            {
                RecuperaForum(Request["id"]).ExcluiForum();
                Response.Redirect("/Inicio/Forum");
            }
            else
                Response.Redirect("/Login/Index");
        }

        public int RecuperaForumId(string id)
        {
            try
            {
                return Convert.ToInt32(System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(id)));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }

        public Models.Forum RecuperaForum(string id)
        {
            Models.Forum forum = new Models.Forum();
            forum.RecuperaForum(RecuperaForumId(id), (Session["UsuarioLogado"] as Models.Usuarios).Id);
            return forum;
        }
    }
}