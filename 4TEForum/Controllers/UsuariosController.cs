﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _4TEForum.Controllers
{
    public class UsuariosController : Controller
    {
        public ActionResult Registrar()
        {
            if (Session["ErroLogin"] != null)
                ViewBag.ErroLogin = Session["ErroLogin"].ToString();

            ViewBag.Title = "Registrar";

            return View();
        }

        [HttpPost]
        public void RegistraUsuario()
        {
            if (Request["senha"] == Request["confirmasenha"])
            {

                Models.Usuarios usuario = new Models.Usuarios();
                usuario.Nome = Request["nome"];
                usuario.Email = Request["email"];
                usuario.Senha = Request["senha"];

                if (usuario.Cadastrado())
                {
                    Session["ErroLogin"] = "E-mail já cadastrado";
                    Response.Redirect("/Usuarios/Registrar");
                }
                else
                {
                    usuario.CadastraUsuario();
                    Session.Remove("ErroLogin");

                    Response.Redirect("/Login/Index");
                }
            }
            else
            {
                Session["ErroLogin"] = "Senhas não coincidem";
                Response.Redirect("/Usuarios/Registrar");
            }
        }
    }
}