﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace _4TEForum.Controllers
{
    public class InicioController : Controller
    {
        public ActionResult Contacto()
        {
            if (Session["UsuarioLogado"] != null)
            {
                ViewBag.Message = "Informações para contacto";
                return View();
            }
            else
                return RedirectToAction("Index", "Login");
        }

        public ActionResult Forum()
        {
            if (Session["UsuarioLogado"] != null)
            {
                ViewBag.Title = $"Bem vind@ ao Forum 4TE, {(Session["UsuarioLogado"] as Models.Usuarios).Nome.Split(' ').First()}";
                var foruns = Models.Forum.RecuperaForuns((Session["UsuarioLogado"] as Models.Usuarios).Id).OrderByDescending(c => c.IncluidoEm);
                ViewBag.Lista = foruns;
                if (foruns.Count() > 0)
                    ViewBag.Message = "Confira os tópicos postados até o momento:";
                else
                    ViewBag.Message = "Não has tópicos postados até o momento.";

                return View();
            }
            else
                return RedirectToAction("Index", "Login");
        }
    }
}