﻿using System.Web.Mvc;

namespace _4TEForum.Controllers
{
    public class SairController : Controller
    {
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
    }
}