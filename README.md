# Forum 4TE
## Execução

Para rodar este projeto, basta executar o script de base de dados chamado "DatabaseScript.sql" presente na solução e configurar a string de conexão na "connectionString" do "web.config".

Há alguns posts e usuários e senhas pré-criados:

- Usuário: andre@fictcio123.com Senha: andre123
- Usuário: rita@ficticio123.com Senha: rita123
- Usuário: vanessa@ficticio123.com Senha: vanessa123
- Usuário: daniel@ficticio123.com Senha: daniel123
- Usuário: ricardo@ficticio123.com Senha: ricardo123

Caso necessário, é possível a criação de novos usuários/posts.

Apenas os proprietários dos posts poderão alterá-los ou excluí-los.
## Authors

- [@adldecker](https://gitlab.com/adldecker)

